import React, { useState, useEffect } from 'react';
import { BrowserRouter, Switch, Route, Redirect  } from 'react-router-dom';
import requireAuth from './authentication/AuthComponent';
/* BOOTSTRAP */
import 'bootstrap/dist/css/bootstrap.css';
/* END */
import './App.css';

/* PAGES */
import  Home from './pages/Home';
import  Dashboard from './pages/Dashboard';
import  NotFound from './pages/NotFound';
/* END */

function App() {

  const [auth, setAuth] = useState(true);

  useEffect(() => {
    if ( ! localStorage.getItem('_access_token_')) {
      setAuth(false);
    }
  }, []);

  return (
    <BrowserRouter>
      <div>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/dashboard' component={requireAuth(Dashboard)} />
          <Route component={NotFound} />
          {!auth &&  <Redirect push to="/"/> }
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
