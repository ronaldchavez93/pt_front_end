import React  from 'react';
/* ROUTER */
import { withRouter } from 'react-router-dom';
/* FIN */

export default function requireAuth(Component) {
    class AuthenticatedComponent extends React.Component {
     constructor(props) {
      super(props);
      this.state = {
       auth: localStorage.getItem('_access_token_')
      }
     }
     componentDidMount() {
      this.checkAuth();
     }
     checkAuth() {
      if ( ! this.state.auth) {
        this.props.history.push(`/NotFound`);
      }
     }
    render() {
      return this.state.auth
       ? <Component { ...this.props } />
       : null;
      }
     }
     return  withRouter(AuthenticatedComponent)
}