import React, { useState, useEffect } from 'react';

/* AXIOS CLIENTE PARA PETICIONES HTTP */
import API from '../config/api';
/* FIN */

/* COMPONENTS REACT BOOTSTRAP */
import {
  Container,
  Navbar,
  Nav,
  Card,
  Row,
  Col,
  Button,
  NavDropdown,
  Badge
} from "react-bootstrap";
/* FIN */

/* COMPONENT */
import  RecargarSaldo  from '../components/RecargarSaldo';
import  Comprar  from '../components/Comprar';
/* END */

/* LIBRERIA PARA MOSTRAR ALERTAS */
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
/* FIN */

/* ROUTER */
import { withRouter } from 'react-router-dom';
/* FIN */

function Dashboard(props) {

  const data = JSON.parse(localStorage.getItem('_data_'));
  const tokenSeguridad = localStorage.getItem('_access_token_');

  const [saldo, setSaldo] = useState(0);
  const [showCompra, setShowCompra] = useState(true);
  
  useEffect(() => {
    getSaldoBilletera();
  }, []);

  function outputEvent(monto) {
    setSaldo(parseFloat(saldo) + parseFloat(monto));
  }

  function outputEventConfirmar(monto) {
    setSaldo(monto);
  }

  async function getSaldoBilletera(){

    const MySwal = withReactContent(Swal);
  
    try {

      let axiosConfig = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : tokenSeguridad
        }
      };
  
      const response = await API.get(`billetera/${data.documento}/${data.celular}`, axiosConfig);
      console.log(response);
      const { billetera } = response.data;
      setSaldo(billetera);
  
    } catch (error) {
  
      if (error.response) {
        console.log(error.response.data);
        console.log(error.response.status);
        const { message } = error.response.data;
        MySwal.fire({
          icon: 'error',
          title: `${message}`,
          showConfirmButton: false,
          timer: 3000
        });
  
      } else if (error.request) {
        console.log(error.request);
      } else {
        console.log('Error', error.message);
      }
  
    }
  }


  function _handleLogout(){
    localStorage.removeItem('_access_token_');
    localStorage.removeItem('_data_');
    props.history.push('/');
  }
  
    return (
        <div>
          <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Navbar.Brand href="#">Billetera digital</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto"></Nav>
              <Nav>
                <Nav.Link onClick={_handleLogout}>
                  Saldo: <Badge variant="success">${saldo.toFixed(2) || 0}</Badge>
                </Nav.Link>
                <NavDropdown title={ data.nombres } id="collasible-nav-dropdown">
                  <NavDropdown.Item onClick={_handleLogout}>Cerrar sesión</NavDropdown.Item>
                </NavDropdown>
              </Nav> 
            </Navbar.Collapse>
          </Navbar>
          <br />
          <Container>

            <Button variant={(showCompra ? 'primary' : 'success')}
                    onClick={() => setShowCompra(!showCompra)}>
              {(showCompra ? 'Recargar' : 'Comprar')}
            </Button>

            {!showCompra &&
            <RecargarSaldo clickHandler={outputEvent} />
            }

            {showCompra &&
            <Comprar handlerConfirmar={outputEventConfirmar} />
            }

          </Container>
          
          
        </div>
    );

}

export default Dashboard;