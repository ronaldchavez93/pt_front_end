import React, { useState } from 'react';

/* COMPONENTS REACT BOOTSTRAP */
import {
    Container,
    Navbar,
    Card,
    Row,
    Col
  } from "react-bootstrap";
  /* FIN */

/* COMPONENT */
import  Login  from '../components/Login';
import  Register  from '../components/Register';
/* END */

function Home() {

    const [showRegister, setShowRegister] = useState(false);

    function outputEvent(event) {
        setShowRegister(false);
    }

    return (
        <div>
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand href="#home">
                    Billetera Digital
                </Navbar.Brand> 
            </Navbar>
            <Container className="mt-4">
                <Row className="mt-1">
                    <Col>
                        <Card   className="card-form shadow center"
                                style={{ width: '40rem' }}>
                            <Card.Header className="text-center">
                                <h4 className="mb-4 text-center">{(showRegister ? 'Registrarse' : 'Inicia sesión')}</h4>
                            </Card.Header>
                            <Card.Body>

                                {showRegister && <Register clickHandler={outputEvent} />}
                                {!showRegister && <Login />}
                                
                            </Card.Body>
                            <Card.Footer className="text-center">
                                <Card.Link  href="#"
                                            onClick={() => setShowRegister(!showRegister)}>{(!showRegister ? 'Registrarse' : 'Inicia sesión')}</Card.Link>
                            </Card.Footer>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default Home;