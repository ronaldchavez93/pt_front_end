import axios from 'axios';

export default axios.create({
  baseURL: `https://pt-wallet.herokuapp.com/api/`
});