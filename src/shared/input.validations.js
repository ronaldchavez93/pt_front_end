const onlyLetters = (e) => {
    let key = e.keyCode || e.which;
    let tecla = String.fromCharCode(key).toLowerCase();
    let letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    if(letras.indexOf(tecla) === -1 && key !== 8)
        e.preventDefault();

}

const onlyNumbers = (e) => {
    let key = e.keyCode || e.which;
    let tecla = String.fromCharCode(key).toLowerCase();
    let number = "0123456789";
    if(number.indexOf(tecla) === -1 && key !== 8)
        e.preventDefault();

}

const onlyLettersAndNumbers = (e) => {
    let key = e.keyCode || e.which;
    let tecla = String.fromCharCode(key).toLowerCase();
    let number = " áéíóúabcdefghijklmnñopqrstuvwxyz0123456789";
    if(number.indexOf(tecla) === -1 && key !== 8)
        e.preventDefault();

}

export default { onlyLetters, 
                 onlyNumbers,
                 onlyLettersAndNumbers };