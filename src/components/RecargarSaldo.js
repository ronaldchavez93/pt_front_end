import React from 'react';

/* AXIOS CLIENTE PARA PETICIONES HTTP */
import API from '../config/api';
/* FIN */

/* VALIDACIONES */
import { Formik } from 'formik';
import * as yup from 'yup';
import _validations from '../shared/input.validations';
/* FIN */

/* COMPONENTS REACT BOOTSTRAP */
import {
    Form,
    Button,
    Row,
    Col,
    Card
  } from "react-bootstrap";
/* FIN */

/* LIBRERIA PARA MOSTRAR ALERTAS */
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
/* FIN */

 /* SCHEMA DE VALIDACIONES FORM RECARGARSALDO */
 const schemaFormRecargarSaldo = yup.object({
    monto: yup.string()
      .trim()
      .required('Campo requerido')
      .min(1, 'Mínimo 1 caracteres')
  });
  
  const formRecargarSaldo = ({
    handleSubmit,
    handleChange,
    handleBlur,
    isSubmitting,
    values,
    touched,
    dirty,
    isValid,
    errors
  }) => (
      <Form noValidate onSubmit={handleSubmit}>
  
        <Form.Group controlId="formMonto">
          <Form.Control
            type="text"
            name="monto"
            value={values.monto}
            onChange={handleChange}
            onBlur={handleBlur}
            isValid={touched.monto && !errors.monto}
            isInvalid={touched.monto && errors.monto}
            onKeyPress={_validations.onlyNumbers}
            placeholder="Introducir monto" />
  
          <Form.Control.Feedback type="invalid"
            className="error">
            {errors.monto}
          </Form.Control.Feedback>
  
        </Form.Group>
  
        <Button variant="secondary"
          type="submit"
          disabled={!dirty || !isValid || isSubmitting}
          block>
          Recargar
        </Button>
  
      </Form>
    );
  
  /* FIN */

function RecargarSaldo(props) {

    const data = JSON.parse(localStorage.getItem('_data_'));
    const tokenSeguridad = localStorage.getItem('_access_token_');

    async function handleRecargar(values, actions){

        const MySwal = withReactContent(Swal);
      
        const { monto } = values;

        try {

            let axiosConfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization' : tokenSeguridad
                }
              };
      
          const response = await API.put(`billetera/recargar`, {   documento: data.documento, 
                                                                    celular: data.celular,
                                                                    saldo: monto }, axiosConfig);
          console.log(response);
          const { message, type } = response.data;
    
          MySwal.fire({
            icon: type,
            title: message,
            showConfirmButton: false,
            timer: 3000
          });

          props.clickHandler(monto);
          values.monto = '';
    
      
        } catch (error) {
      
          if (error.response) {
            console.log(error.response.data);
            console.log(error.response.status);
            const { message } = error.response.data;
            MySwal.fire({
              icon: 'error',
              title: `${message}`,
              showConfirmButton: false,
              timer: 3000
            });
      
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
      
        }
    }

    return (
        <div>
            <Row className="mt-1">
                    <Col>
                        <Card   className="card-form shadow center"
                                style={{ width: '40rem' }}>
                            <Card.Header className="text-center">
                                <h4 className="mb-4 text-center">Recargar saldo</h4>
                            </Card.Header>
                            <Card.Body>

                            <Formik validationSchema={schemaFormRecargarSaldo}
                                    onSubmit={handleRecargar}
                                    initialValues={{
                                        monto: ''
                                    }}
                                    component={formRecargarSaldo} />
                                
                            </Card.Body>
                        </Card>
                    </Col>
            </Row>
          
        </div>
    );

}

export default RecargarSaldo;

