import React from 'react';

/* AXIOS CLIENTE PARA PETICIONES HTTP */
import API from '../config/api';
/* FIN */

/* VALIDACIONES */
import { Formik } from 'formik';
import * as yup from 'yup';
import _validations from '../shared/input.validations';
/* FIN */

/* COMPONENTS REACT BOOTSTRAP */
import {  Form, 
          Col, 
          Button } from "react-bootstrap";
/* FIN */

/* LIBRERIA PARA MOSTRAR ALERTAS */
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
/* FIN */

/* SCHEMA DE VALIDACIONES FORM REGISTRO */
const schemaFormRegistro = yup.object({
  nombres: yup.string()
          .trim()
          .required('Campo requerido')
          .min(2, 'Mínimo 2 caracteres')
          .max(30, 'Máximo 30 caracteres'),
  documento: yup.string()
          .trim()
          .required('Campo requerido')
          .min(4, 'Mínimo 4 caracteres')
          .max(15, 'Máximo 15 caracteres'),
  email: yup.string()
          .trim()
          .required('Campo requerido')
          .email('Formato de email incorrecto')
          .min(10, 'Mínimo 10 caracteres')
          .max(50, 'Máximo 50 caracteres'),
  celular: yup.string()
          .trim()
          .required('Campo requerido')
          .min(4, 'Mínimo 4 caracteres')
          .max(15, 'Máximo 15 caracteres'),
  clave: yup.string()
          .trim()
          .required('Campo requerido')
          .matches(/[a-z]/, "La clave debe contener al menos una letra minúscula (a-z)")
          .matches(/[A-Z]/, "La clave debe contener al menos una letra mayuscula (A-Z)")
          .matches(/[0-9]/, "La clave debe contener al menos un número (0-9)")
          .matches(/[!@#$%&*()+_]/, "La clave debe contener al menos un carácter especial (! @ # $ % & * () + _ )")
          .min(8, 'Mínimo 8 caracteres')
          .max(12, 'Máximo 12 caracteres'),
  confirmacionClave: yup.string()
          .oneOf([yup.ref('clave'), null], 'Las claves deben coincidir')
          .required('Campo requerido')
});
  
/* FIN */

const formRegistro = ({
  handleSubmit,
  handleChange,
  handleBlur,
  isSubmitting,
  values,
  touched,
  dirty,
  isValid,
  errors
}) => (
<Form noValidate onSubmit={handleSubmit}>

<Form.Row>
<Form.Group as={Col} controlId="formGridPn">
<Form.Label>
  Nombres <strong className="text-danger">*</strong>
</Form.Label>
<Form.Control   type="text" 
              name="nombres"
              value={values.nombres}
              onChange={handleChange}
              onBlur={handleBlur}
              isValid={touched.nombres && !errors.nombres}
              isInvalid={touched.nombres && errors.nombres}
              onKeyPress={_validations.onlyLetters}
              placeholder="Ingresar nombres" />

<Form.Control.Feedback type="invalid" 
         className="error">
  {errors.nombres}
</Form.Control.Feedback>

</Form.Group>

<Form.Group as={Col} controlId="formGridSn">
<Form.Label>
  Documento <strong className="text-danger">*</strong>
</Form.Label>
<Form.Control type="text"
              name="documento"
              value={values.documento}
              onChange={handleChange}
              onBlur={handleBlur}
              isValid={touched.documento && !errors.documento}
              isInvalid={touched.documento && errors.documento} 
              onKeyPress={_validations.onlyNumbers}
              placeholder="Ingresar documento"
              maxLength="15" />

<Form.Control.Feedback type="invalid" 
         className="error">
  {errors.documento}
</Form.Control.Feedback>

</Form.Group>
</Form.Row>

<Form.Row>
<Form.Group as={Col} controlId="formGridPa">
<Form.Label>
Email <strong className="text-danger">*</strong>
</Form.Label>
<Form.Control type="text" 
              name="email"
              value={values.email}
              onChange={handleChange}
              onBlur={handleBlur}
              isValid={touched.email && !errors.email}
              isInvalid={touched.email && errors.email}
              placeholder="Ingresar email"
              maxLength="50" />

<Form.Control.Feedback type="invalid" 
         className="error">
  {errors.email}
</Form.Control.Feedback>

</Form.Group>

<Form.Group as={Col} controlId="formGridSa">
<Form.Label>
Celular <strong className="text-danger">*</strong>
</Form.Label>
<Form.Control type="text"
              name="celular"
              value={values.celular}
              onChange={handleChange}
              onBlur={handleBlur}
              isValid={touched.celular && !errors.celular}
              isInvalid={touched.celular && errors.celular} 
              onKeyPress={_validations.onlyNumbers}
              placeholder="Ingresar celular"
              maxLength="15" />

<Form.Control.Feedback type="invalid" 
         className="error">
  {errors.celular}
</Form.Control.Feedback>

</Form.Group>
</Form.Row>

<Form.Row>
<Form.Group as={Col} controlId="formGridC">
<Form.Label>
Clave <strong className="text-danger">*</strong>
</Form.Label>
<Form.Control type="password"
              name="clave"
              onChange={handleChange}
              onBlur={handleBlur}
              isValid={touched.clave && !errors.clave}
              isInvalid={touched.clave && errors.clave} 
              placeholder="Crear clave"
              maxLength="12" />

<Form.Control.Feedback type="invalid" 
         className="error">
  {errors.clave}
</Form.Control.Feedback>

</Form.Group>

<Form.Group as={Col} controlId="formGridCc">
<Form.Label>
Confirmar clave <strong className="text-danger">*</strong>
</Form.Label>
<Form.Control   type="password"
              name="confirmacionClave"
              onChange={handleChange}
              onBlur={handleBlur}
              isValid={touched.confirmacionClave && !errors.confirmacionClave}
              isInvalid={touched.confirmacionClave && errors.confirmacionClave}  
              placeholder="Confirmar clave" />

<Form.Control.Feedback type="invalid" 
         className="error">
  {errors.confirmacionClave}
</Form.Control.Feedback>

</Form.Group>
</Form.Row>

<Button variant="secondary" 
type="submit"
disabled={!dirty || !isValid || isSubmitting}
block>
 Registrarse
</Button>
</Form>
);

function Registro(props) {

  async function handleRegister(values, actions){

    const MySwal = withReactContent(Swal);
  
    const { nombres, 
            documento,
            email,
            celular,
            clave } = values;

    try {
  
      const response = await API.post(`cliente`, {  nombres: nombres, 
                                                    documento: documento,
                                                    email: email,
                                                    celular: celular,
                                                    clave: clave });
      console.log(response);
      const { message, type } = response.data;

      MySwal.fire({
        icon: type,
        title: message,
        showConfirmButton: false,
        timer: 1500
      });

      props.clickHandler();
  
    } catch (error) {
  
      if (error.response) {
        console.log(error.response.data);
        console.log(error.response.status);
        const { message } = error.response.data;
        MySwal.fire({
          icon: 'error',
          title: `${message}`,
          showConfirmButton: false,
          timer: 1500
        });
  
      } else if (error.request) {
        console.log(error.request);
      } else {
        console.log('Error', error.message);
      }
  
    }
  }

  return (
    <div>
      <Formik validationSchema={schemaFormRegistro}
              onSubmit={handleRegister}
              initialValues={{
                nombres: '',
                documento: '',
                email: '',
                celular: '',
                clave: '',
                confirmacionClave: ''
              }}
              component={formRegistro} />
    </div>
  );
}

export default Registro;