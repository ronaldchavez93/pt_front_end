import React, { useState } from 'react';

/* AXIOS CLIENTE PARA PETICIONES HTTP */
import API from '../config/api';
/* FIN */

/* VALIDACIONES */
import { Formik } from 'formik';
import * as yup from 'yup';
import _validations from '../shared/input.validations';
/* FIN */

/* COMPONENTS REACT BOOTSTRAP */
import {
    Form,
    Button,
    Row,
    Col,
    Card
  } from "react-bootstrap";
/* FIN */

/* LIBRERIA PARA MOSTRAR ALERTAS */
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
/* FIN */

 /* SCHEMA DE VALIDACIONES FORM COMPRAR */
 const schemaFormComprar = yup.object({
    monto: yup.string()
      .trim()
      .required('Campo requerido')
      .min(1, 'Mínimo 1 caracteres')
  });
  
  const formComprar = ({
    handleSubmit,
    handleChange,
    handleBlur,
    isSubmitting,
    values,
    touched,
    dirty,
    isValid,
    errors
  }) => (
      <Form noValidate onSubmit={handleSubmit}>
  
        <Form.Group controlId="formMonto">
          <Form.Control
            type="text"
            name="monto"
            value={values.monto}
            onChange={handleChange}
            onBlur={handleBlur}
            isValid={touched.monto && !errors.monto}
            isInvalid={touched.monto && errors.monto}
            onKeyPress={_validations.onlyNumbers}
            placeholder="Introducir monto" />
  
          <Form.Control.Feedback type="invalid"
            className="error">
            {errors.monto}
          </Form.Control.Feedback>
  
        </Form.Group>
  
        <Button variant="secondary"
          type="submit"
          disabled={!dirty || !isValid || isSubmitting}
          block>
          Comprar
        </Button>
  
      </Form>
    );
  
/* FIN */

 /* SCHEMA DE VALIDACIONES FORM CONFIRMAR COMPRA */
 const schemaFormConfirmar = yup.object({
    token: yup.string()
      .trim()
      .required('Campo requerido')
      .min(6, 'Mínimo 6 caracteres'),
  });
  
  const formConfirmar = ({
    handleSubmit,
    handleChange,
    handleBlur,
    isSubmitting,
    values,
    touched,
    dirty,
    isValid,
    errors
  }) => (
      <Form noValidate onSubmit={handleSubmit}>

        <Form.Group controlId="formToken">
          <Form.Control
            type="text"
            name="token"
            value={values.token}
            onChange={handleChange}
            onBlur={handleBlur}
            isValid={touched.token && !errors.token}
            isInvalid={touched.token && errors.token}
            onKeyPress={_validations.onlyLettersAndNumbers}
            placeholder="Introducir token" />
  
          <Form.Control.Feedback type="invalid"
            className="error">
            {errors.token}
          </Form.Control.Feedback>
  
        </Form.Group>
  
        <Button variant="secondary"
          type="submit"
          disabled={!dirty || !isValid || isSubmitting}
          block>
          Confirmar Compra
        </Button>
  
      </Form>
    );
  
/* FIN */




function Comprar(props) {

    const data = JSON.parse(localStorage.getItem('_data_'));
    const tokenSeguridad = localStorage.getItem('_access_token_');

    const [compra, setCompra] = useState('');

    async function handleComprar(values, actions){

        const MySwal = withReactContent(Swal);
      
        const { monto } = values;

        try {

            let axiosConfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization' : tokenSeguridad
                }
              };
      
          const response = await API.post(`billetera/pagar`, {  documento: data.documento, 
                                                                celular: data.celular,
                                                                monto: monto }, axiosConfig);
          console.log(response);
          const { message, type, compra } = response.data;
          setCompra(compra._id);

          MySwal.fire({
            icon: type,
            title: message,
            showConfirmButton: false,
            timer: 3000
          });
    
      
        } catch (error) {
      
          if (error.response) {
            console.log(error.response.data);
            console.log(error.response.status);
            const { message } = error.response.data;
            MySwal.fire({
              icon: 'error',
              title: `${message}`,
              showConfirmButton: false,
              timer: 3000
            });
      
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
      
        }
    }

    async function handleConfirmar(values, actions){

        const MySwal = withReactContent(Swal);
      
        const { token } = values;

        try {

            let axiosConfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization' : tokenSeguridad
                }
              };
      
          const response = await API.put(`billetera/confirmar-pago`, {   documento: data.documento, 
                                                                         celular: data.celular,
                                                                         token: token,
                                                                         compra: compra }, axiosConfig);
          console.log(response);
          const { message, type, saldo } = response.data;

          MySwal.fire({
            icon: type,
            title: message,
            showConfirmButton: false,
            timer: 3000
          });

          props.handlerConfirmar(saldo);
          setCompra('');
    
      
        } catch (error) {
      
          if (error.response) {
            console.log(error.response.data);
            console.log(error.response.status);
            const { message } = error.response.data;
            MySwal.fire({
              icon: 'error',
              title: `${message}`,
              showConfirmButton: false,
              timer: 3000
            });
      
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
      
        }
    }

    

    return (
        <div>
            <Row className="mt-1">
                    <Col>
                        <Card   className="card-form shadow center"
                                style={{ width: '40rem' }}>
                            <Card.Header className="text-center">
                                <h4 className="mb-4 text-center">
                                    { (compra != '' ? 'Confirmar compra': 'Comprar') }
                                </h4>
                            </Card.Header>
                            <Card.Body>
                                {compra == '' &&
                                <Formik validationSchema={schemaFormComprar}
                                        onSubmit={handleComprar}
                                        initialValues={{
                                            monto: ''
                                        }}
                                        component={formComprar} />
                                }

                                {compra != '' &&
                                <Formik validationSchema={schemaFormConfirmar}
                                        onSubmit={handleConfirmar}
                                        initialValues={{
                                            token: ''
                                        }}
                                        component={formConfirmar} />
                                }
                                
                            </Card.Body>
                        </Card>
                    </Col>
            </Row>
          
        </div>
    );

}

export default Comprar;