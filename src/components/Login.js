import React from 'react';

/* AXIOS CLIENTE PARA PETICIONES HTTP */
import API from '../config/api';
/* FIN */

/* VALIDACIONES */
import { Formik } from 'formik';
import * as yup from 'yup';
/* FIN */

/* COMPONENTS REACT BOOTSTRAP */
import {
  Form,
  Button
} from "react-bootstrap";
/* FIN */

/* LIBRERIA PARA MOSTRAR ALERTAS */
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
/* FIN */

/* ROUTER */
import { withRouter } from 'react-router-dom';
/* FIN */

 /* SCHEMA DE VALIDACIONES FORM LOGIN */
 const schemaFormLogin = yup.object({
  email: yup.string()
    .trim()
    .required('Campo requerido')
    .email('Formato de Correo electrónico incorrecto')
    .min(10, 'Mínimo 10 caracteres')
    .max(50, 'Máximo 50 caracteres'),
  clave: yup.string()
    .trim()
    .required('Campo requerido')
    .min(8, 'Mínimo 8 caracteres')
    .max(15, 'Máximo 15 caracteres')
});

const formLogin = ({
  handleSubmit,
  handleChange,
  handleBlur,
  isSubmitting,
  values,
  touched,
  dirty,
  isValid,
  errors
}) => (
    <Form noValidate onSubmit={handleSubmit}>

      <Form.Group controlId="formEmail">
        <Form.Control
          type="text"
          name="email"
          value={values.email}
          onChange={handleChange}
          onBlur={handleBlur}
          isValid={touched.email && !errors.email}
          isInvalid={touched.email && errors.email}
          placeholder="Introducir email" />

        <Form.Control.Feedback type="invalid"
          className="error">
          {errors.email}
        </Form.Control.Feedback>

      </Form.Group>

      <Form.Group controlId="formClave">
        <Form.Control
          type="password"
          name="clave"
          onChange={handleChange}
          onBlur={handleBlur}
          isValid={touched.clave && !errors.clave}
          isInvalid={touched.clave && errors.clave}
          placeholder="Introducir clave" />

        <Form.Control.Feedback type="invalid" className="error">
          {errors.clave}
        </Form.Control.Feedback>

      </Form.Group>

      <Button variant="secondary"
        type="submit"
        disabled={!dirty || !isValid || isSubmitting}
        block>
        Iniciar sesión
      </Button>

    </Form>
  );

/* FIN */


function Login(props) {

  async function handleLogin(values, actions){

    const MySwal = withReactContent(Swal);
  
    const { email, 
            clave } = values;
    try {
  
      const response = await API.post(`iniciar-sesion`, { email: email, clave: clave });
      console.log(response);
      const { title, token, type, cliente } = response.data;

      MySwal.fire({
        icon: type,
        title: title,
        showConfirmButton: false,
        timer: 1500
      });

      /*Add localStorage*/
      localStorage.setItem('_access_token_', token);
      localStorage.setItem('_data_', JSON.stringify(cliente));
      /* Fin */

      props.history.push('/dashboard');
  
    } catch (error) {
  
      if (error.response) {
        console.log(error.response.data);
        console.log(error.response.status);
        const { message } = error.response.data;
        MySwal.fire({
          icon: 'error',
          title: `${message}`,
          showConfirmButton: false,
          timer: 1500
        });
  
      } else if (error.request) {
        console.log(error.request);
      } else {
        console.log('Error', error.message);
      }
  
    }
  }

  return (
    <div>
      <Formik validationSchema={schemaFormLogin}
              onSubmit={handleLogin}
              initialValues={{
                email: '',
                clave: ''
              }}
              component={formLogin} />
    </div>
  );
}

export default withRouter(Login);